namespace ScrumRatingFront.Shared
{
    public static class Urls
    {
        public const string getSummary = "api/summary/";
        public const string searchTeam = "/api/teams/GetByName?name=";
        public const string getTeamById = "/api/team/GetById?id=";
        public const string getStudentsByTeamId = "/api/team/GetByTeamId?id=";
        public const string getStudentRange = "/api/student/Range";
        public const string addStudentToTeam = "/api/team/AddStudents?teamId=";
        public const string getTeam = "/api/team/All?pageSize=10&pageNumber=1";
    }
}