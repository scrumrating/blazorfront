﻿using System;
using Newtonsoft.Json;

namespace ScrumRatingFront.Shared.Models
{
    /// <summary>
    /// Урок.
    /// </summary>
    public class Lesson
    {
        /// <summary>
        /// Идентификатор записи.
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }

        /// <summary>
        /// Тема урока.
        /// </summary>
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        /// <summary>
        /// Описание урока.
        /// </summary>
        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        /// <summary>
        /// Дата и время начала урока
        /// </summary>
        [JsonProperty(PropertyName = "dateLesson")]
        public DateTime DateLesson { get; set; }

        /// <summary>
        /// Идентификатор домашнего задания.
        /// </summary>
        [JsonProperty(PropertyName = "homeworkId")]
        public int? HomeworkId { get; set; }

        /// <summary>
        /// Идентификатор учебного курса.
        /// </summary>
        [JsonProperty(PropertyName = "courseId")]
        public int? CourseId { get; set; }

        public override string ToString()
        {
            return $"Id = '{this.Id}', Name = '{this.Name}', DateLesson = '{this.DateLesson}'";
        }
    }
}