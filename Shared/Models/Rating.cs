﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScrumRatingFront.Shared.Models
{
    public class Rating
    {
        /// <summary>
        /// Идентификатор оценки
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }
        /// <summary>
        /// Идентификатор студента
        /// </summary>
        [JsonProperty(PropertyName = "StudentId")]
        public int StudentId { get; set; }
        /// <summary>
        /// Тип оценки (1 - за ДЗ, 2 - за урок)
        /// </summary>
        [JsonProperty(PropertyName = "LinkKind")]
        public int LinkKind { get; set; }
        /// <summary>
        /// Ссылка на "Оценка за ДЗ" или "Оценка за урок"
        /// </summary>
        [JsonProperty(PropertyName = "LinkId")]
        public int? LinkId { get; set; }
       
        /// <summary>
        /// Значение оценки
        /// </summary>
        [JsonProperty(PropertyName = "Value")]
        public int Value { get; set; }
    }
}
