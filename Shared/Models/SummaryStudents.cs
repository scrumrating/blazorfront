namespace ScrumRatingFront.Shared.Models
{
    public class SummaryStudents
    {
        public int[] StrudentList { get; set; }
        public int[] LessonList { get; set; }
        public int[] HomeWorkList { get; set; }
    }
}