namespace ScrumRatingFront.Shared.Models
{
    public class GetSummaryTeamListViewModel
    {
        public TeamDto[] TeamList { get; set; }
        public int[] LessonList { get; set; }
        public int[] HomeWorkList { get; set; }
    }
}