﻿using System;
using Newtonsoft.Json;

namespace ScrumRatingFront.Shared.Models
{
    [Serializable]
    public class Student
    {
        [JsonProperty(PropertyName = "id")] 
        public int Id { get; set; }
        
        [JsonProperty(PropertyName = "firstName")]
        public string Firstname { get; set; }
        
        [JsonProperty(PropertyName = "middleName")]
        public string MiddleName { get; set; }
        
        [JsonProperty(PropertyName = "lastName")]
        public string LastName { get; set; }
        
        [JsonProperty(PropertyName = "email")]
        public string Email { get; set; }
    }
}
