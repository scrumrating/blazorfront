using System;
using System.Collections.Generic;

namespace ScrumRatingFront.Shared.Models
{
    public class Summary
    {
        public Dictionary<string, double> StudentsByRating { get; set; }
        public Dictionary<string, double> TeamsByRating { get; set; }

        public Summary()
        {
            StudentsByRating = new Dictionary<string, double>();
            TeamsByRating = new Dictionary<string, double>();
        }
    }
}