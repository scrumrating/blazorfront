namespace ScrumRatingFront.Shared.Models
{
    public class StudentsWithRatingResult
    {
        public int Id { get; set; }
        public string LastName { get; set; }
        public string Firstname { get; set; }
        public string Email { get; set; }
        public int LessonValue { get; set; }
        public int HomeWorkValue { get; set; }
        public int LinkId { get; set; }
        public int RatingId { get; set; }

        public override string ToString()
        {
            return $"Id = {Id}, Email = {Email}, LessonValue = {LessonValue}, HomeWorkValue = {HomeWorkValue}, LinkId = {LinkId}, RatingId = {RatingId}";
        }
    }
}