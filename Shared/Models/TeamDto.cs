namespace ScrumRatingFront.Shared.Models
{
    public class TeamDto
    {
        public int Id { get; set; }
        public int[] Students { get; set; }
    }
}