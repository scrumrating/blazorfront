﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScrumRatingFront.Shared.Models
{
    public class Course
    {
        /// <summary>
        /// Идентификатор записи.
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }

        /// <summary>
        /// Наименвоание курса.
        /// </summary>
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        /// <summary>
        /// Описание урока.
        /// </summary>
        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        /// <summary>
        /// Дата начала курса
        /// </summary>
        [JsonProperty(PropertyName = "startDate")]
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Дата окончания курса
        /// </summary>
        [JsonProperty(PropertyName = "finishDate")]
        public DateTime FinishDate { get; set; }
        

        public override string ToString()
        {
            return $"Id = '{this.Id}', Name = '{this.Name}', ({this.StartDate} -{this.FinishDate})";
        }
    }
}
