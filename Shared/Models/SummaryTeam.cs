namespace ScrumRatingFront.Shared.Models
{
    public class SummaryTeam
    {
        public int TeamId { get; set; }
        public int SumRating { get; set; }
    }
}