﻿using System;
using Newtonsoft.Json;

namespace ScrumRatingFront.Shared.Models
{
    /// <summary>
    /// Домашнее задание.
    /// </summary>
    public class Homework
    {
        /// <summary>
        /// Получает или задаёт идентификатор записи.
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }

        /// <summary>
        /// Получает или задаёт название домашнего задания.
        /// </summary>
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        /// <summary>
        /// Получает или задаёт описание домашнего задания.
        /// </summary>
        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        /// <summary>
        /// Получает или задаёт множетель домашнего задания.
        /// </summary>
        [JsonProperty(PropertyName = "factor")]
        public int Factor { get; set; }

        /// <summary>
        /// Получает или задаёт id урока.
        /// </summary>
        [JsonProperty(PropertyName = "lessonId")]
        public int LessonId { get; set; }

        /// <summary>
        /// Получает или задаёт дату сдачи домашнего задания.
        /// </summary>
        [JsonProperty(PropertyName = "deadLine")]
        public DateTime DeadLine { get; set; }

        public override string ToString()
        {
            return $"Id = '{this.Id}', Name = '{this.Name}', DeadLine = '{this.DeadLine}', Factor = '{this.Factor}', LessonId = '{this.LessonId}', Description = '{this.Description}'";
        }
    }
}