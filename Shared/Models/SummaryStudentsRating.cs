namespace ScrumRatingFront.Shared.Models
{
    public class SummaryStudentsRating
    {
        public int StudentId { get; set; }
        public int SumRating { get; set; }
    }
}