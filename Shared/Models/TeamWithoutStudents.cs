using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace ScrumRatingFront.Shared.Models
{
    public class TeamWithoutStudents
    {
        [JsonProperty(PropertyName = "id")] 
        public int Id { get; set; }
        [Required, JsonProperty(PropertyName = "name")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 5)]
        public string Name { get; set; }
        [Required, JsonProperty(PropertyName = "overallRating")]
        public double OverallRating { get; set; }
    }
}